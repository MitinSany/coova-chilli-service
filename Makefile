include $(TOPDIR)/rules.mk

PKG_NAME:=coova-chilli-service
PKG_RELEASE=2

#PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)

include $(INCLUDE_DIR)/package.mk

define Package/$(PKG_NAME)
	CATEGORY:=Wifi-Party
	TITLE:=Wifi-Party Coova-Chilli hotspot service
	URL:=https://bitbucket.org/MitinSany/coova-chilli-service
	DEPENDS:=+coova-chilli
endef

define Package/$(PKG_NAME)/description
	Wifi-Party Coova-Chilli hotspot service
endef

define Package/$(PKG_NAME)/postinst
#!/bin/sh
/etc/init.d/chilli enable
/etc/init.d/chilli start
endef

define Package/$(PKG_NAME)/prerm
#!/bin/sh
/etc/init.d/chilli stop
/etc/init.d/chilli disable
endef

define Build/Compile
#	$(MAKE) CC="$(TARGET_CC)" CFLAGS="$(TARGET_CFLAGS)" -C $(PKG_BUILD_DIR)
endef

define Package/$(PKG_NAME)/install
	$(INSTALL_DIR) $(1)/etc/init.d
	$(INSTALL_BIN) ./files/chilli $(1)/etc/init.d/chilli
endef

$(eval $(call BuildPackage,$(PKG_NAME)))
